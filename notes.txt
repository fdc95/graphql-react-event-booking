
followed tutorial: 
https://github.com/academind/yt-graphql-react-event-booking-api
https://www.youtube.com/playlist?list=PL55RiY5tL51rG1x02Yyj93iypUuHYXcB_


-- init project (locally) --

> match project's node & npm versions (see package.json's "engines")
> npm i
> create .env file using .env.example as template


=================================================


-- TODO --

switch to FaunaDB:
> see docs
> create new repo on Github
> use VS Code ext?
> go easy on schema validations at 1st, TypegraphQL and 
	class-validator pkg might help w/ that


-- TODO later on --

use typegraphQL?
auth w/ Firebase (update .gitignore)


-- improvements --

switch to apollo-server? (what about Yoga or the others?)


-- packages --

move to dependencies?:
copyfiles
cors (usage code edits needed if it stays as devDep)


